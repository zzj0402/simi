import { Injectable } from "@angular/core";
import * as UniversalSentenceEncoder from "@tensorflow-models/universal-sentence-encoder";
import * as math from "mathjs";

@Injectable({
  providedIn: "root"
})
export class SemanticSimilarityService {
  constructor() {}

  async compare(text1: string, text2: string): Promise<number> {
    let model = await UniversalSentenceEncoder.load();
    let embeddings = await model.embed([text1, text2]);
    let embeddingsArray = await embeddings.array();
    let similarityScore = Math.round(
      math.dot(embeddingsArray[0], embeddingsArray[1]) * 100
    );
    return similarityScore;
  }
}
