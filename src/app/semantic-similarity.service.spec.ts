import { TestBed, fakeAsync, tick } from "@angular/core/testing";

import { SemanticSimilarityService } from "./semantic-similarity.service";

describe("SemanticSimilarityService", () => {
  let originalTimeout = 0;
  beforeEach(() => {
    TestBed.configureTestingModule({});
    originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 100000;
  });

  it("should return a similarity score for two text strings", async () => {
    const service: SemanticSimilarityService = TestBed.get(
      SemanticSimilarityService
    );
    let similarityScore = await service.compare(
      "This is a test!",
      "This is a test!"
    );
    expect(similarityScore).toBe(100, "Same strings should return 100!");
    similarityScore = await service.compare(
      "This is another test!",
      "This is a test!"
    );
    expect(similarityScore).toBeLessThan(
      100,
      "Similar strings do not return 100!"
    );
    similarityScore = await service.compare(
      "This is another test!",
      "Hello, how are you?"
    );
    expect(similarityScore).toBeLessThan(
      50,
      "Different strings should return less than 50!"
    );
  });
  afterEach(function() {
    jasmine.DEFAULT_TIMEOUT_INTERVAL = originalTimeout;
  });
});
