![Build Status](https://gitlab.com/pages/plain-html/badges/master/build.svg)

**Table of Contents**

- [Goal](#Goal)
- [Use Cases](#Use-Cases)
- [Did you fork this project?](#did-you-fork-this-project)
- [Troubleshooting](#troubleshooting)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Goal

Building the best sentence similarity comparing algorithm.

## Use Cases

### Answer Checking

As a rote learning method, checking with the right answer is frequently on-demand.
Submit an answer statement and check it with the sample answer.
The web app returns a similarity percentage.
